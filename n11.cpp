#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <vector>

struct Vec2i
{
  int x;
  int y;

  Vec2i & operator+=(const Vec2i rhs)
  {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }
};

Vec2i operator+(const Vec2i lhs, const Vec2i rhs) { return {lhs.x + rhs.x, lhs.y + rhs.y}; }

constexpr std::array<Vec2i, 8U> d2q8 = {Vec2i{-1, -1}, Vec2i{0, -1}, Vec2i{1, -1}, Vec2i{-1, 0},
                                        Vec2i{1, 0},   Vec2i{-1, 1}, Vec2i{0, 1},  Vec2i{1, 1}};

class Map
{
 public:
  Map() = default;
  Map(std::istream & is)
  {
    std::string line;
    if (!std::getline(is, line))
      return;
    width_ = line.size();
    widthWithGl_ = width_ + 2;
    map_.insert(map_.end(), widthWithGl_, '.');
    do
    {
      map_.push_back('.');
      std::copy(line.begin(), line.end(), std::back_inserter(map_));
      map_.push_back('.');
      ++height_;
    } while (std::getline(is, line));
  }

  char & operator()(Vec2i x) { return map_[(x.y + 1) * widthWithGl_ + (x.x + 1)]; }
  const char & operator()(Vec2i x) const { return map_[(x.y + 1) * widthWithGl_ + (x.x + 1)]; }

  bool isSeat(Vec2i x) const { return (*this)(x) != '.'; }
  bool isOccupied(Vec2i x) const { return (*this)(x) == '#'; }
  bool isEmpty(Vec2i x) const { return (*this)(x) == 'L'; }

  bool hasAdjacentOccipiedSeats(const Vec2i x) const
  {
    // return std::any_of(d2q8.begin(), d2q8.end(), [&](const Vec2i d) { return isOccupied(x + d); });

    for (auto dir : d2q8)
    {
      for (Vec2i y = x + dir; isInDomain(y); y += dir)
      {
        if (isOccupied(y))
          return true;
        if (isEmpty(y))
          break;
      }
    }

    return false;
  }

  int numAdjacentOccipiedSeats(Vec2i x) const
  {
    // return std::count_if(d2q8.begin(), d2q8.end(), [&](const Vec2i d) { return isOccupied(x + d); });
    int num = 0;
    for (auto dir : d2q8)
    {
      for (Vec2i y = x + dir; isInDomain(y); y += dir)
      {
        if (isOccupied(y))
        {
          ++num;
          break;
        }
        if (isEmpty(y))
          break;
      }
    }

    return num;
  }

  int countOccupied() const { return std::count(map_.begin(), map_.end(), '#'); }

  int height() const { return height_; }
  int width() const { return width_; }
  bool isInDomain(Vec2i x) const { return x.x >= 0 && x.y >= 0 && x.x < width_ && x.y < height_; }

 private:
  int width_ = 0;
  int height_ = 0;
  int widthWithGl_ = 0;
  std::vector<char> map_;
};

std::ostream & operator<<(std::ostream & os, Map & map)
{
  for (Vec2i x{0, 0}; x.y < map.height(); ++x.y)
  {
    for (x.x = 0; x.x < map.width(); ++x.x)
    {
      std::cout << map(x);
    }
    std::cout << "\n";
  }

  return os;
}

bool iterate(Map & map, Map & tmp)
{
  bool changed = false;
  for (Vec2i x{0, 0}; x.y < map.height(); ++x.y)
    for (x.x = 0; x.x < map.width(); ++x.x)
    {
      if (map.isEmpty(x) && !map.hasAdjacentOccipiedSeats(x))
      {
        tmp(x) = '#';
        changed = true;
      }
      else if (map.isOccupied(x) && map.numAdjacentOccipiedSeats(x) >= 5)
      {
        tmp(x) = 'L';
        changed = true;
      }
      else
        tmp(x) = map(x);
    }

  std::swap(map, tmp);

  return changed;
}

int main()
{
  Map map(std::cin);
  Map tmp = map;

  std::cout << map << "\n";
  int i = 0;
  while (iterate(map, tmp))
  {
    std::cout << map << "\n";
    //std::cout << map({0, 8}) << " " << map.numAdjacentOccipiedSeatsDbg({0, 8}) << "\n";
    //std::cout << map({0, 9}) << " " << map.numAdjacentOccipiedSeatsDbg({0, 8}) << "\n";
    // if (i++ > 10)
    //   break;
  }

  std::cout << map.countOccupied() << "\n";
}