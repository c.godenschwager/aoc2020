#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

struct Seat {
  Seat(const std::string &s) {

    if(s.size() < 9U)
        throw std::runtime_error("invalid string");
    
    for (int i = 0; i < 7; ++i) 
      row += s[i] == 'F' ? 0 : 1 << (6 - i);

    for (int i = 0; i < 3; ++i) 
      column += s[7+i] == 'L' ? 0 : 1 << (2 - i);    
  }

  int id() const { return row * 8 + column; }

  int row = 0;
  int column = 0;
};

std::ostream & operator<<(std::ostream & os, Seat & s)
{
    return os << s.row << " " << s.column << " " << s.id();
}


int main() {
    std::vector<Seat> seats;
    for(std::string line; std::getline(std::cin, line);)
        seats.emplace_back(line);

    std::sort(seats.begin(), seats.end(), [](const Seat & lhs, const Seat & rhs){ return lhs.id() < rhs.id(); });

    for(auto it = seats.begin(); it != seats.end(); ++it)
    {
        if(std::next(it)->id() != it->id() + 1)
            std::cout << it->id() + 1 << std::endl;
    }

}
