#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

struct Pwd
{
  int min;
  int max;
  char c;
  std::string s;
};

std::istream & operator>>(std::istream & is, Pwd & p)
{
  char dummy;
  is >> p.min >> dummy >> p.max >> p.c >> dummy >> p.s;
  return is;
}

std::ostream & operator<<(std::ostream & os, Pwd & p)
{
  os << "\"" << p.s << "\""
     << " must have " << p.min << " to " << p.max << " '" << p.c << "'";
  return os;
}

int main()
{
  int counter = 0;
  for (;;)
  {
    Pwd p;
    std::cin >> p;
    if (!std::cin)
      break;
    --(p.min);
    --(p.max);
    if ((p.min < p.s.size() && p.s[p.min] == p.c) == (p.max < p.s.size() && p.s[p.max] == p.c))
    {
      std::cout << p << std::endl;
    }
    else
    {
      ++counter;
    }
  }

  std::cout << counter << std::endl;
}
