#include <iostream>
#include <unordered_map>
#include <vector>

int main()
{
  const std::vector<int> startingNumbers = {1, 2, 16, 19, 18, 0};
  const size_t numTurns = 30000000U;
  std::vector<int> numbers;
  std::vector<int> turnMap(numTurns, -1);

  numbers.push_back(startingNumbers.front());
  for (size_t turn = 1U; turn < startingNumbers.size(); ++turn)
  {
    turnMap[numbers.back()] = turn - 1U;
    numbers.push_back(startingNumbers[turn]);
  }

  for (size_t turn = startingNumbers.size(); turn < numTurns; ++turn)
  {
    const auto lastNumber = numbers.back();
    if (turnMap[lastNumber] == -1)
      numbers.push_back(0);
    else
      numbers.push_back(turn - turnMap[lastNumber] - 1U);
    turnMap[lastNumber] = turn - 1U;
  }

  std::cout << numbers.back() << "\n";
}