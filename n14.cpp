#include <bitset>
#include <iostream>
#include <map>
#include <regex>
#include <variant>

struct MaskInstruction
{
  uint64_t floatMask = 0U;
  uint64_t orMask = 0U;
};

std::ostream & operator<<(std::ostream & os, const MaskInstruction & i)
{
  std::cout << std::bitset<36>(i.floatMask) << " " << std::bitset<36>(i.orMask);
  return os;
}

struct MemInstruction
{
  uint64_t address = 0U;
  uint64_t value = 0U;
};

std::ostream & operator<<(std::ostream & os, const MemInstruction & i)
{
  std::cout << i.address << " " << i.value;
  return os;
}

using Instruction = std::variant<MaskInstruction, MemInstruction>;

std::vector<Instruction> parseInput(std::istream & is)
{
  static const std::regex maskRegex{"mask = ([01X]{36})"};
  static const std::regex memRegex{"mem\\[(\\d+)\\] = (\\d+)"};

  std::vector<Instruction> result;

  for (std::string line; std::getline(std::cin, line);)
  {
    std::string bits;
    if (std::smatch s; std::regex_match(line, s, maskRegex))
    {
      MaskInstruction instr;
      bits = s[1];
      for (uint64_t i = 0; i < 36U; ++i)
        switch (bits[i])
        {
          case 'X':
            instr.floatMask |= uint64_t(1) << (uint64_t(35) - i);
            break;
          case '1':
            instr.orMask |= uint64_t(1) << (uint64_t(35) - i);
            break;
          case '0':
            break;
          default:
            throw std::runtime_error("Error parsing mask \"" + line + "\"");
        }
      result.emplace_back(instr);
    }
    else if (std::smatch s; std::regex_match(line, s, memRegex))
    {
      result.emplace_back(MemInstruction{std::stoull(s[1]), std::stoull(s[2])});
    }
    else
      throw std::runtime_error("Error parsing \"" + line + "\"");
  }
  return result;
}

template <class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

int main()
{
  std::vector<Instruction> instructions = parseInput(std::cin);

  std::map<uint64_t, uint64_t> memory;
  uint64_t floatMask = 0U;
  uint64_t orMask = 0U;

  std::vector<uint64_t> addresses;
  std::vector<uint64_t> tmpAddresses;

  for (const auto & i : instructions)
  {
    //std::visit([](auto && v) { std::cout << v << "\n"; }, i);
    std::visit(overloaded{[&](const MaskInstruction & i) {
                            floatMask = i.floatMask;
                            orMask = i.orMask;
                          },
                          [&](const MemInstruction & i) {
                            addresses.clear();
                            addresses.push_back(i.address | orMask);
                              for (uint64_t i = 0; i < 36; ++i)
                              if (floatMask & (uint64_t(1) << i))
                              {
                                tmpAddresses.clear();
                                for (const auto & a : addresses)
                                  tmpAddresses.push_back(a ^ (uint64_t(1) << i));
                                addresses.insert(addresses.end(), tmpAddresses.begin(),
                                                 tmpAddresses.end());
                              }

                            for (const auto & a : addresses)
                            {
                              //std::cout << a << "\n";
                              memory[a] = i.value;
                            }
                          }},
               i);
  }

  uint64_t sum = 0U;
  for (const auto & [k, v] : memory)
    sum += v;

  std::cout << sum << "\n";
}