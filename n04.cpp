#include <iostream>
#include <regex>
#include <string>

bool byrIsValid(const std::string & s)
{
  static const std::regex byrRegex{"byr\\:(\\d{4})(\\s|$)"};
  if (std::smatch m; std::regex_search(s, m, byrRegex))
  {
    const int year = std::stoi(m[1]);
    if (year >= 1920 && year <= 2002)
      return true;
  }
  return false;
}

bool iyrIsValid(const std::string & s)
{
  static const std::regex iyrRegex{"iyr\\:(\\d{4})(\\s|$)"};
  if (std::smatch m; std::regex_search(s, m, iyrRegex))
  {
    const int year = std::stoi(m[1]);
    if (year >= 2010 && year <= 2020)
      return true;
  }
  return false;
}

bool eyrIsValid(const std::string & s)
{
  static const std::regex eyrRegex{"eyr\\:(\\d{4})(\\s|$)"};
  if (std::smatch m; std::regex_search(s, m, eyrRegex))
  {
    const int year = std::stoi(m[1]);
    if (year >= 2020 && year <= 2030)
      return true;
  }
  return false;
}

bool hgtIsValid(const std::string & s)
{
  static const std::regex hgtRegex{"hgt\\:(\\d+)(cm|in)(\\s|$)"};
  if (std::smatch m; std::regex_search(s, m, hgtRegex))
  {
    int height = std::stoi(m[1]);
    if (m[2] == "cm" && height >= 150 && height <= 193)
      return true;

    if (m[2] == "in" && height >= 59 && height <= 76)
      return true;
  }
  return false;
}

bool hclIsValid(const std::string & s)
{
  static const std::regex hclRegex{"hcl\\:\\#[0-9a-f]{6}()(\\s|$)"};
  return std::regex_search(s, hclRegex);
}

bool eclIsValid(const std::string & s)
{
  static const std::regex eclRegex{"ecl\\:(amb|blu|brn|gry|grn|hzl|oth)(\\s|$)"};
  return std::regex_search(s, eclRegex);
}

bool pidIsValid(const std::string & s)
{
  static const std::regex pidRegex{"pid\\:\\d{9}(\\s|$)"};
  return std::regex_search(s, pidRegex);
}

int main()
{
  int ctr = 0;
  for (std::string cardString; std::cin;)
  {
    cardString.clear();
    for (std::string line; std::getline(std::cin, line);)
    {
      if (std::all_of(line.begin(), line.end(), isspace))
        break;
      if (!cardString.empty())
        cardString += ' ';
      cardString += line;
    }

    if (byrIsValid(cardString) && iyrIsValid(cardString) && eyrIsValid(cardString) &&
        hgtIsValid(cardString) && hclIsValid(cardString) && eclIsValid(cardString) &&
        pidIsValid(cardString))
      ++ctr;
  }

  std::cout << ctr << std::endl;
}