#include <iostream>
#include <regex>
#include <string>
#include <map>

struct ContainedBags
{
  std::string name;
  size_t num;
};

using ContainmentMap = std::multimap<std::string, ContainedBags>;


void parseLine(const std::string & s, ContainmentMap & containmentMap)
{
  static const std::regex noBagsRegex{"([a-z ]+) bags contain no other bags\\."};
  static const std::regex bagsRegex{"([a-z ]+) bags contain .*"};
  static const std::regex bagsBackRegex{"(\\d+) ([a-z ]+) bag"};

  if(std::smatch m; std::regex_match(s, m, noBagsRegex))
  {
    const std::string & color = m[1];
  }
  else if(std::smatch m; std::regex_match(s, m, bagsRegex))
  {
    const std::string & color = m[1];
    for(auto it = std::sregex_iterator(s.begin(), s.end(), bagsBackRegex); it != std::sregex_iterator(); ++it)
    {
      const std::smatch & matches = *it;
      containmentMap.emplace(color, ContainedBags{matches[2], std::stoul(matches[1])});
    }
    
  }
}

size_t getContainers(const ContainmentMap & containmentMap, const std::string & s)
{
  const auto range = containmentMap.equal_range(s);
  size_t result = 1U;
  for(auto it = range.first; it != range.second; ++it)
    result += it->second.num * getContainers(containmentMap, it->second.name);
  return result;
}

int main()
{
  ContainmentMap containmentMap;
  for (std::string line; std::getline(std::cin, line);)
    parseLine(line, containmentMap);
  
  std::cout << getContainers(containmentMap, "shiny gold") - 1U << "\n";
}