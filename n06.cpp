#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

std::istream &getSetLine(std::istream &is, std::string &line) {
  if (!std::getline(std::cin, line))
    return is;

  std::sort(line.begin(), line.end());
  line.erase(std::unique(line.begin(), line.end()), line.end());
  return is;
}

int main() {
  int sum = 0;
  while (std::cin) {

    std::string answers;
    if(!getSetLine(std::cin, answers))
        continue;

    for (std::string newAnswers; getSetLine(std::cin, newAnswers);) {
      if (newAnswers.empty())
        break;

      std::string tmp;
      std::set_intersection(answers.begin(), answers.end(), newAnswers.begin(),
                            newAnswers.end(), std::back_inserter(tmp));
      answers = std::move(tmp);
    }
    sum += answers.size();
  }

  std::cout << sum << std::endl;
}
