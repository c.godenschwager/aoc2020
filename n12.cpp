#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <ostream>
#include <vector>

struct Instruction {
  char dir;
  int value;
};

std::istream &operator>>(std::istream &os, Instruction &inst) {
  return os >> inst.dir >> inst.value;
}

std::ostream &operator<<(std::ostream &os, const Instruction &inst) {
  return os << inst.dir << " " << inst.value;
}

const std::map<int, int> sin = {{0, 0}, {90, 1}, {180, 0}, {270, -1}};
const std::map<int, int> cos = {{0, 1}, {90, 0}, {180, -1}, {270, 0}};

int main() {
  std::vector<Instruction> instructions;
  std::copy(std::istream_iterator<Instruction>(std::cin),
            std::istream_iterator<Instruction>(),
            std::back_inserter(instructions));

  // for(auto i : instructions)
  //{
  //    std::cout << i << "\n";
  //}

  int wptX = 10;
  int wptY = -1;
  int shipX = 0;
  int shipY = 0;
  int tmp = 0;
  for (auto i : instructions) {
    switch (i.dir) {
    case 'N':
      wptY -= i.value;
      break;
    case 'S':
      wptY += i.value;
      break;
    case 'E':
      wptX += i.value;
      break;
    case 'W':
      wptX -= i.value;
      break;
    case 'R':
      // curDir = (curDir + i.value) % 360;
      tmp = wptX;
      wptX = wptX * cos.at(i.value) - wptY * sin.at(i.value);
      wptY = tmp * sin.at(i.value) + wptY * cos.at(i.value);
      break;
    case 'L':
      // curDir = (curDir + 360 - i.value) % 360;
      tmp = wptX;
      wptX = wptX * cos.at(360 - i.value) - wptY * sin.at(360 - i.value);
      wptY = tmp * sin.at(360 - i.value) + wptY * cos.at(360 - i.value);
      break;
    case 'F':
      //   switch (curDir) {
      //   case 0:
      //     wptY += i.value;
      //     break;
      //   case 90:
      //     wptX += i.value;
      //     break;
      //   case 180:
      //     wptY -= i.value;
      //     break;
      //   case 270:
      //     wptX -= i.value;
      //     break;
      //   };
      shipX += i.value * wptX;
      shipY += i.value * wptY;
      break;
    };
    std::cout << i << " " << shipX << " " << shipY << " - " << wptX << " "
              << wptY << std::endl;
  }

  std::cout << std::abs(shipX) + std::abs(shipY) << std::endl;
}
