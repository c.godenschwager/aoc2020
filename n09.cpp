#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

template <typename It>
bool isSumOf(const It first, const It last, const int64_t number)
{
  for (auto it = first; it != last; ++it)
    for (auto it2 = std::next(it); it2 != last; ++it2)
      if (*it + *it2 == number)
        return true;
  return false;
}

template <typename It>
std::pair<It, It> findSumRange(const It first, const It last, const int64_t number)
{
  auto it0 = first;
  auto it1 = std::next(first, 2);
  int64_t sum = std::accumulate(it0, it1, int64_t(0));

  auto inc = [&]() {
    sum += *it1;
    ++it1;
  };

  auto dec = [&]() {
    sum -= *it0;
    ++it0;
    if (std::distance(it0, it1) < 2)
      inc();
  };

  while (it1 != last)
  {
    if (sum < number)
      inc();
    else if (sum > number)
      dec();
    else 
      break;
  }

  if(sum == number)
    return {it0, it1};

  return {first, first};
}

constexpr int64_t length = 25;

int main()
{
  std::vector<int64_t> values{std::istream_iterator<int64_t>(std::cin), std::istream_iterator<int64_t>()};

  for (auto it = std::next(values.begin(), length); it != values.end(); ++it)
  {
    if (!isSumOf(std::prev(it, length), it, *it))
    {
      auto its = findSumRange(values.begin(), values.end(), *it);
      
      auto eles = std::minmax_element(its.first, its.second);
      std::cout << *it << " " << *eles.first + *eles.second << " " << std::distance(values.begin(), it) << std::endl;
      return 0;
    }
  }
}
