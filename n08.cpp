#include <cerrno>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

struct Instruction
{
  Instruction(const std::string & s)
  {
    static const std::regex instructionRegex{"(nop|acc|jmp) ((:?\\+|\\-)\\d+)"};
    if (std::smatch m; std::regex_match(s, m, instructionRegex))
    {
      inst = m[1];
      value = std::stoi(m[2]);
    }
    else
      throw std::runtime_error("Bad instruction: " + s);
  }

  std::string inst;
  int value;
};

class Program
{
 public:
  Program() = default;
  Program(std::istream & is)
  {
    for (std::string line; std::getline(is, line);)
      instructions_.emplace_back(line);
  }

  void fix(int instIdx)
  {
    if (instructions_[instIdx].inst == "nop")
      instructions_[instIdx].inst = "jmp";
    else if (instructions_[instIdx].inst == "jmp")
      instructions_[instIdx].inst = "nop";
  }

  int numInstructions() const { return instructions_.size(); }

  bool run()
  {
    const int numInstructions = instructions_.size();
    int i = 0U;
    std::vector<bool> visited(instructions_.size(), false);
    int akku = 0;
    while (i < numInstructions)
    {
      if (visited[i])
        return false;

      visited[i] = true;
      Instruction & inst = instructions_[i];
      if (inst.inst == "nop")
      {
        ++i;
      }
      else if (inst.inst == "acc")
      {
        akku += inst.value;
        ++i;
      }
      else
      {
        i += inst.value;
      }
    }
    if (i != numInstructions)
      return false;

    std::cout << akku << "\n";
    return true;
  }

 private:
  std::vector<Instruction> instructions_;
};

int main()
{
  const Program original(std::cin);
  Program copy;
  for (int i = 0; i < original.numInstructions(); ++i)
  {
    copy = original;
    copy.fix(i);
    if (copy.run())
      break;
  }
}