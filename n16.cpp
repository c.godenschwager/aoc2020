#include <iostream>
#include <regex>
#include <set>
#include <vector>

using Ticket = std::vector<int>;

Ticket parseTicket(const std::string & str)
{
  std::vector<int> tokens;
  size_t prev = 0, pos = 0;
  do
  {
    pos = str.find(',', prev);
    if (pos == std::string::npos)
      pos = str.length();
    tokens.push_back(std::stoi(str.substr(prev, pos - prev)));
    prev = pos + 1U;
  } while (pos < str.length() && prev < str.length());
  return tokens;
}

struct Interval
{
  int min = 0;
  int max = 0;

  bool isValid(const int x) const { return x >= min && x <= max; }
};

std::ostream & operator<<(std::ostream & os, const Interval & i) { return os << i.min << "-" << i.max; }

struct TwoIntervals
{
  Interval a, b;

  bool isValid(const int x) const { return a.isValid(x) || b.isValid(x); }
};

std::ostream & operator<<(std::ostream & os, const TwoIntervals & i) { return os << i.a << " or " << i.b; }

struct InputData
{
  std::map<std::string, TwoIntervals> intervals;
  Ticket myTicket;
  std::vector<Ticket> nearbyTickets;
};

std::ostream & operator<<(std::ostream & os, const InputData & i)
{
  for (const auto & [name, intervals] : i.intervals)
    os << name << ": " << intervals << "\n";
  os << "\n";

  return os;
}

struct InputData parseInoutData(std::istream & is)
{
  static std::regex intervalRegex("([a-z ]+)\\: (\\d+)-(\\d+) or (\\d+)-(\\d+)");

  InputData result;
  std::string line;
  for (; std::getline(std::cin, line) && line != "";)
  {
    if (std::smatch s; std::regex_match(line, s, intervalRegex) && s.size() == 6U)
    {
      result.intervals[s[1]] = {{std::stoi(s[2]), std::stoi(s[3])}, {std::stoi(s[4]), std::stoi(s[5])}};
    }
    else
    {
      throw std::runtime_error("Bad interval line");
    }
  }
  std::getline(std::cin, line);
  std::getline(std::cin, line);
  result.myTicket = parseTicket(line);

  std::getline(std::cin, line);
  std::getline(std::cin, line);

  for (; std::getline(std::cin, line) && line != "";)
  {
    result.nearbyTickets.push_back(parseTicket(line));
  }

  return result;
}

int main()
{
  InputData input = parseInoutData(std::cin);

  std::vector<Ticket> validTickets;
  int sum = 0;
  for (const auto & t : input.nearbyTickets)
  {
    bool ticketValid = true;
    for (const int x : t)
      if (!std::any_of(input.intervals.begin(), input.intervals.end(),
                       [x](const auto & intervals) { return intervals.second.isValid(x); }))
      {
        ticketValid = false;
        sum += x;
      }
    if (ticketValid)
      validTickets.push_back(t);
  }

  std::cout << sum << "\n";

  std::set<std::string> allFields;
  for (const auto & [name, intervals] : input.intervals)
    allFields.insert(name);

  std::vector<std::set<std::string>> fieldCandidates(input.intervals.size(), allFields);
  for (const auto & t : validTickets)
    for (size_t i = 0; i < t.size(); ++i)
      for (const auto & [name, intervals] : input.intervals)
        if (!intervals.isValid(t[i]))
          fieldCandidates[i].erase(name);

  for (size_t numErased = 1U; numErased > 0U;)
  {
    numErased = 0U;
    for (size_t i = 0; i < fieldCandidates.size(); ++i)
      if (fieldCandidates[i].size() == 1U)
      {
        for (size_t j = 0; j < i; ++j)
          numErased += fieldCandidates[j].erase(*fieldCandidates[i].begin());
        for (size_t j = i + 1U; j < fieldCandidates.size(); ++j)
          numErased += fieldCandidates[j].erase(*fieldCandidates[i].begin());
      }
  }

  // for (const auto & c : fieldCandidates)
  //{
  //  for (const auto & s : c)
  //    std::cout << s << " ";
  //  std::cout << "\n";
  //}

  int64_t product = 1;
  for (size_t i = 0; i < fieldCandidates.size(); ++i)
    if (fieldCandidates[i].begin()->starts_with("departure"))
      product *= input.myTicket[i];

  std::cout << product << "\n";
}