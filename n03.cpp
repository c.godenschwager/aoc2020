#include <array>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

class Map
{
 public:
  Map(std::istream & is)
  {
    for (std::string line; std::getline(is, line);)
    {
      std::copy(line.begin(), line.end(), std::back_inserter(map_));
      width_ = line.size();
      ++height_;
    }
  }

  char operator()(int x, int y) const { return map_[y * width_ + x % width_]; }

  int height() const { return height_; }
  int width() const { return width_; }

 private:
  int width_ = 0;
  int height_ = 0;
  std::vector<char> map_;
};

int main()
{
  Map map(std::cin);

  std::array<int, 5U> xSlopes = {1, 3, 5, 7, 1};
  std::array<int, 5U> ySlopes = {1, 1, 1, 1, 2};
  std::array<long int, 5U> numTrees = {0, 0, 0, 0, 0};

  for (int i = 0; i < 5; ++i)
  {
    for (int x = 0, y = 0; y <= map.height(); x += xSlopes[i], y += ySlopes[i])
    {
      if (map(x, y) == '#')
        ++numTrees[i];
    }
  }

  std::cout << std::accumulate(numTrees.begin(), numTrees.end(), 1L, std::multiplies<long int>())
            << std::endl;
}