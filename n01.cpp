#include <iostream>
#include <iterator>
#include <vector>

int main()
{
  std::vector<int> values{std::istream_iterator<int>(std::cin), std::istream_iterator<int>()};

  for (auto it = values.begin(); it != values.end(); ++it)
    for (auto it2 = std::next(it); it2 != values.end(); ++it2)
      for (auto it3 = std::next(it2); it3 != values.end(); ++it3)
        if (*it + *it2 + *it3 == 2020)
          std::cout << *it << " * " << *it2 << " * " << *it3 << " = " << *it * *it2 * *it3 << "\n";
}