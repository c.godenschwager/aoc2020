#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

size_t dynamic(const std::vector<int> & adapters)
{
  std::vector<size_t> combos(adapters.size(), 0U);
  combos.front() = 1U;

  for(size_t i = 0; i < adapters.size(); ++i)
  {
    const int maxJolt = adapters[i] + 3;
    for(size_t j = i + 1; j < adapters.size(); ++j)
    {
      if(adapters[j] > maxJolt)
        break;
      combos[j] += combos[i];
    }
  }

  return combos.back();
}

int main()
{
  std::vector<int> adapters{std::istream_iterator<int>(std::cin), std::istream_iterator<int>()};

  std::sort(adapters.begin(), adapters.end());
  adapters.push_back(adapters.back() + 3);

  std::vector<int> jolt_diff;
  std::adjacent_difference(adapters.begin(), adapters.end(), std::back_inserter(jolt_diff));

  int64_t diff1count = std::count(jolt_diff.begin(), jolt_diff.end(), 1);
  int64_t diff3count = std::count(jolt_diff.begin(), jolt_diff.end(), 3);

  std::cout << diff1count << " " << diff3count << " " << diff1count * diff3count << "\n";

  adapters.insert(adapters.begin(), 0);

  std::cout << dynamic(adapters) << "\n";
}
