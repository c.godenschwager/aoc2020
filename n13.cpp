#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <omp.h>
#include <ostream>
#include <vector>

std::vector<std::string> split(const std::string &str,
                               const std::string &delim) {
  std::vector<std::string> tokens;
  size_t prev = 0, pos = 0;
  do {
    pos = str.find(delim, prev);
    if (pos == std::string::npos)
      pos = str.length();
    tokens.push_back(str.substr(prev, pos - prev));
    prev = pos + delim.length();
  } while (pos < str.length() && prev < str.length());
  return tokens;
}

int64_t timeofNextBus(const int64_t t, const int64_t line) {
  return ((t + line - 1) / line) * line;
}

int64_t waitTime(const int64_t t, const int64_t line) {
  return timeofNextBus(t, line) - t;
}

struct Line {
  int64_t l;
  int64_t i;
};

int main() {
  int64_t mint64_time = 0;
  std::string inputLines;
  std::cin >> mint64_time >> inputLines;

  auto stringLines = split(inputLines, ",");
  std::vector<Line> busLines;
  for (int i = 0; i < stringLines.size(); ++i)
    if (stringLines[i] != "x")
      busLines.push_back(Line{std::stoi(stringLines[i]), i});

  //   auto bestLine = std::min_element(busLines.begin(), busLines.end(),
  //                                    [&](int64_t lhs, int64_t rhs) {
  //                                      return timeofNextBus(mint64_time, lhs)
  //                                      <
  //                                             timeofNextBus(mint64_time,
  //                                             rhs);
  //                                    });

  //   std::cout << "Best line is " << *bestLine << " with wait time "
  //             << waitTime(mint64_time, *bestLine) << " taking the bus at "
  //             << timeofNextBus(mint64_time, *bestLine) << " solution "
  //             << waitTime(mint64_time, *bestLine) * *bestLine << "\n";

  std::sort(busLines.begin(), busLines.end(),
            [](const auto &lhs, const auto &rhs) { return lhs.l > rhs.l; });

  int64_t initialT = busLines.front().l - busLines.front().i;
  while (initialT < 0)
    initialT += busLines.front().l;


#pragma omp parallel for schedule(dynamic, 1024 * 1024)
    for (int64_t t = initialT;
         t < std::numeric_limits<int64_t>::max() - busLines.front().l;
         t += busLines.front().l) {

      bool solutionOK = true;
      for (int64_t i = 0; i < busLines.size(); ++i) {
        if ((t + busLines[i].i) % busLines[i].l != 0) {
          solutionOK = false;
          break;
        }
      }
      if (solutionOK) {
        std::cout << "Solution t = " << t << "\n";
      }
    }
  }

  // 327300950120029 (10^15)